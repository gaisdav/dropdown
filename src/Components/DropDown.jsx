import React, {useState, useEffect, useRef} from "react";
import "./DropDown.scss";

/**
 * Константы для клавиш.
 */
const KEY_DOWN = 40;
const KEY_UP = 38;
const KEY_ESC = 27;
const KEY_ENTER = 13;

/**
 * Компонент DropDown.
 */
const DropDown = (props) => {
    const [showList, openList] = useState(false);
    const [selectedList, changeSelectedList] = useState([]);
    const [activeItemIndex, changeActiveItemIndex] = useState(0);
    const [inputValue, changeInputValue] = useState('');
    const [inputInFocus, changeInputFocus] = useState(false);
    const [list, changeList] = useState(props.list);

    const dropDownEl = useRef(null);
    const inputEl = useRef(null);

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);
        document.addEventListener("keydown", handleKeyDown);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
            document.removeEventListener("keydown", handleKeyDown);
        };
    });

    /**
     * Захлопывает список при клике за пределами компонента.
     */
    const handleClickOutside = (e) => {
        if (!dropDownEl.current.contains(e.target)) {
            openList(false);
        }
    };

    /**
     * Обработчики клавиш.
     */
    const handleKeyDown = (e) => {
        if (showList) {
            if (e.keyCode === KEY_UP && activeItemIndex > 0) {
                changeActiveItemIndex(activeItemIndex - 1)
            } else if (e.keyCode === KEY_DOWN && activeItemIndex < list.length - 1) {
                changeActiveItemIndex(activeItemIndex + 1)
            } else if (e.keyCode === KEY_ESC) {
                openList(false);
            } else if (e.keyCode === KEY_ENTER && !!list[activeItemIndex]) {
                handleSelectItem(list.filter((item) => item.text.toLocaleLowerCase().includes(inputValue.toLocaleLowerCase()))[activeItemIndex].id);
                changeActiveItemIndex(0)
            }
        } else {
            if (e.keyCode === KEY_DOWN && inputInFocus) {
                openList(true)
            }
        }
    };

    /**
     * Выбор элемента списка. Добавляет выбранный элемент в список выбранных и удаляет из общего списка.
     */
    const handleSelectItem = (itemId) => {
        if (!!selectedList.find(item => item.id === itemId)) {

            const newSelectedList = selectedList.filter(item => item.id !== itemId);
            changeSelectedList(newSelectedList);

            const newList = [...list, props.list.find(item => item.id === itemId)];
            changeList(newList);
        } else {

            const newSelectedList = [...selectedList, {
                id: itemId,
                text: list.find(item => item.id === itemId).text,
            }];
            changeSelectedList(newSelectedList);

            const newList = list.filter(item => item.id !== itemId);
            changeList(newList)
        }

        openList(false);
        changeInputValue('');
        changeActiveItemIndex(0);
    };

    /**
     * Обработчик изменений поля ввода текста.
     */
    const handleChangeInputValue = (e) => {
        const value = e.target.value;
        changeInputValue(value);
        openList(true);
    };

    /**
     * Раскрывает список при получении фокуса.
     */
    const handleInputFocus = () => {
        openList(true);
        changeInputFocus(true)
    };

    /**
     * Возвращает значение фокуса при его исчезновении.
     */
    const handleInputBlur = () => {
        changeInputFocus(false);
        changeActiveItemIndex(0);
    };

    return (
        <div className={`drop-down ${showList ? "drop-down_opened" : ""}`} ref={dropDownEl}>
            <div className="drop-down__input-wrapper">

                {selectedList.map(item => (
                    <div key={item.id} className={'selected-item'}>
                        {item.text}
                        <div className="delete-item" onClick={() => handleSelectItem(item.id)}/>
                    </div>
                ))}
                <div className="drop-down__input">

                    <input
                        ref={inputEl}
                        className={"drop-down__input"}
                        placeholder={props.placeholder}
                        type="text"
                        onFocus={handleInputFocus}
                        onBlur={handleInputBlur}
                        onChange={handleChangeInputValue}
                        value={inputValue}
                    />

                    <div
                        className="drop-down__arrow"
                        onClick={() => openList(!showList)}
                    />
                </div>
            </div>

            {showList && (
                <ul className="drop-down__list">
                    {
                        !!list.find((item) => {
                            return item.text.toLocaleLowerCase().includes(inputValue.toLocaleLowerCase())
                        })
                            ?
                            list
                                .filter((item) => item.text.toLocaleLowerCase().includes(inputValue.toLocaleLowerCase()))
                                .map((item, index) => (
                                        <li
                                            key={item.id}
                                            data-index={index}
                                            className={activeItemIndex === index ? 'hover' : ''}
                                            onMouseOver={() => changeActiveItemIndex(index)}
                                            onClick={() => handleSelectItem(item.id, index)}
                                        >
                                            {item.text}
                                        </li>
                                    )
                                ) : (<li>Ничего не найдено</li>)
                    }
                </ul>
            )}
        </div>
    );
};

export default DropDown;
