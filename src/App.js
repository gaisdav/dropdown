import React from 'react';
import './App.css';
import DropDown from "./Components/DropDown";

const authors = [
    {
        id: 1,
        text: "Федор Достоевский"
    },
    {
        id: 2,
        text: "Джек Лондон"
    },
    {
        id: 3,
        text: "Стивен Кинг"
    },
    {
        id: 4,
        text: "Лев Толстой"
    },
    {
        id: 5,
        text: "Александр Пушкин"
    },
    {
        id: 6,
        text: "Антон Чехов"
    },
    {
        id: 7,
        text: "Эрих Мария Ремарк"
    },
    {
        id: 8,
        text: "Александр Дюма"
    },
    {
        id: 9,
        text: "Агата Кристи"
    },
    {
        id: 10,
        text: "Марк Твен"
    },
    {
        id: 11,
        text: "Жюль Верн"
    }
];

function App() {
  return (
    <div className="App">
      <DropDown list={authors} placeholder={'Выберите автора'}/>
    </div>
  );
}

export default App;
